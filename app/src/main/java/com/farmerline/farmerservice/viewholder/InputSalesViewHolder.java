package com.farmerline.farmerservice.viewholder;

import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.farmerline.farmerservice.R;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class InputSalesViewHolder extends RecyclerView.ViewHolder {

    public TextView tvName;
    public TextView tvPercentage;
    public TextView tvAmount;
    public ImageView imgAbrev;
    public ProgressBar pbPercentage;

    public InputSalesViewHolder(@NonNull View itemView) {
        super(itemView);

        tvName = itemView.findViewById(R.id.tv_name);
        tvPercentage = itemView.findViewById(R.id.tv_percentage);
        tvAmount = itemView.findViewById(R.id.tv_amount);
        imgAbrev = itemView.findViewById(R.id.img_abrev);
        pbPercentage = itemView.findViewById(R.id.pb_percentage);

    }
}
