package com.farmerline.farmerservice.model;

public class InputSales {

    private String name;
    private String percentage;
    private String amount;
    private String color;

    public InputSales(String name, String percentage, String amount, String color) {
        this.name = name;
        this.percentage = percentage;
        this.amount = amount;
        this.color = color;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPercentage() {
        return percentage;
    }

    public void setPercentage(String percentage) {
        this.percentage = percentage;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
