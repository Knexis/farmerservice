package com.farmerline.farmerservice.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ClipDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.amulyakhare.textdrawable.TextDrawable;
import com.farmerline.farmerservice.model.InputSales;
import com.farmerline.farmerservice.viewholder.InputSalesViewHolder;
import com.farmerline.farmerservice.R;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class InputSalesAdapter extends RecyclerView.Adapter<InputSalesViewHolder> {

    private List<InputSales> entities;
    private Context context;

    public InputSalesAdapter(List<InputSales> entities, Context context) {
        this.entities = entities;
        this.context = context;
    }

    @Override
    public InputSalesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_input_sales, parent, false);
        return new InputSalesViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull InputSalesViewHolder holder, int position) {
        bindItem(holder, entities.get(position));
    }

    private void bindItem(InputSalesViewHolder holder, final InputSales entity) {

        holder.tvName.setText(entity.getName());
        holder.tvPercentage.setText(getPercentage(entity.getPercentage()));
        holder.tvAmount.setText(getAmount(entity.getAmount()));
        holder.imgAbrev.setImageDrawable(getDrawable(entity.getName(), entity.getColor()));
        updateProgress(Integer.parseInt(entity.getPercentage()), entity.getColor(), holder.pbPercentage);

    }

    @Override
    public int getItemCount() {
        return entities.size();
    }

    private Drawable getDrawable(String name, String color){

        TextDrawable drawable = TextDrawable.builder()
                .buildRound(getDrawableText(name), Color.parseColor(color));

        return drawable;

    }

    private String getDrawableText(String name){
        String[] strings = name.split(" ");
        StringBuilder builder = new StringBuilder();

        for(String s: strings){
            builder.append(s.charAt(0));
        }

        return builder.toString();
    }

    private String getPercentage(String percentage){
        return percentage+"%";
    }

    private String getAmount(String amount){
        return "Ghs"+amount;
    }

    private void updateProgress(int oldProgress, String color, ProgressBar progressBar){

        // define new drawable/colour
        final float[] roundedCorners = new float[]
                { 5, 5, 5, 5, 5, 5, 5, 5 };
        ShapeDrawable shape = new ShapeDrawable(new RoundRectShape(
                roundedCorners, null, null));

        shape.getPaint().setColor(Color.parseColor(color));
        ClipDrawable clip = new ClipDrawable(shape, Gravity.LEFT,
                ClipDrawable.HORIZONTAL);
        progressBar.setProgressDrawable(clip);

        progressBar.setBackgroundDrawable(context.getDrawable(
                R.drawable.bg_progress_bar));

        // work around: setProgress() ignores a change to the same value
        progressBar.setProgress(0);
        progressBar.setProgress(oldProgress);
    }

}
