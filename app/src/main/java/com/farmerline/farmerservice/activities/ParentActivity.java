package com.farmerline.farmerservice.activities;

import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;

import com.farmerline.farmerservice.R;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import static android.app.Notification.EXTRA_TITLE;

public abstract class ParentActivity extends AppCompatActivity implements
        OnMapReadyCallback{

    private static final String TAG = ParentActivity.class.getSimpleName();
    protected Toolbar toolbar;

    private GoogleMap map;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());

        setUpToolbar();

        initViews(savedInstanceState);

        SupportMapFragment mapFragment =
                (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);

        if(mapFragment != null)
            mapFragment.getMapAsync(this);
    }

    private void setUpToolbar(){
        toolbar = findViewById(R.id.toolbar);
        try{setSupportActionBar(toolbar);}catch (Exception e){/**/}

        String title = getIntent().getStringExtra(EXTRA_TITLE);
        ActionBar bar = getSupportActionBar();
        if (title != null && bar != null) {
            bar.setTitle(title);
            bar.setDisplayHomeAsUpEnabled(true);
            bar.setDisplayShowHomeEnabled(true);
            bar.setHomeButtonEnabled(true);
            toolbar.setTitle(title);
        }
        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);

        }catch (Exception e){
            Log.d(TAG,e.toString());
        }

    }

    protected abstract int getLayoutId();

    protected abstract void initViews(Bundle savedInstanceState);

    @Override
    public void onMapReady(GoogleMap googleMap) {

        map = googleMap;

        map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                onMapClicked();
            }
        });
    }

    public void onMapClicked() {}

    public void animateToMap(final LatLng point) {
        CameraPosition mapanimator =
                new CameraPosition.Builder().target(point)
                        .zoom(15.5f)
                        .tilt(70)
                        .build();
        changeCamera(CameraUpdateFactory.newCameraPosition(mapanimator), new GoogleMap.CancelableCallback() {

            @Override
            public void onFinish() {

                addMarker(point);

            }

            @Override
            public void onCancel() {}
        });
    }

    private void changeCamera(CameraUpdate update, GoogleMap.CancelableCallback callback) {
        map.animateCamera(update, callback);
    }

    private void addMarker(LatLng position){
        map.addMarker(new MarkerOptions().
                position(position).
                icon(BitmapDescriptorFactory.fromBitmap(
                        BitmapFactory.decodeResource(
                                getResources(),R.drawable.ic_selected_location))).
                title(null).
                snippet(null));
    }


}
