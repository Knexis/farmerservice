package com.farmerline.farmerservice.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.farmerline.farmerservice.model.InputSales;
import com.farmerline.farmerservice.adapter.InputSalesAdapter;
import com.farmerline.farmerservice.fragment.InputSalesGraphFragment;
import com.farmerline.farmerservice.R;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.material.navigation.NavigationView;

import androidx.annotation.Nullable;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.MenuItem;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends ParentActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private int REQUEST_PICK_LOCATION = 99;

    private RecyclerView rvInputSales;
    private InputSalesAdapter inputSalesAdapter;
    private View cvAddLocation;

    public static void start(Activity activity) {
        if(activity == null)
            return;

        activity.startActivity(new Intent(activity, MainActivity.class));
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    protected void initViews(Bundle savedInstanceState) {

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().add(R.id.container_input_sales_graph, new InputSalesGraphFragment()).commit();
        }

        rvInputSales = findViewById(R.id.rv_input_sales);
        rvInputSales.setLayoutManager(new LinearLayoutManager(this));
        initInputSales();

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        cvAddLocation = findViewById(R.id.cv_add_location);
        cvAddLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startPickLocationActivity();

            }
        });

    }

    private void startPickLocationActivity() {
        PickLocationActivity.startForResult(MainActivity.this, REQUEST_PICK_LOCATION);
    }

    @Override
    public void onMapClicked() {
        super.onMapClicked();
        PickLocationActivity.startForResult(MainActivity.this, REQUEST_PICK_LOCATION);
    }

    private void initInputSales(){

        List<InputSales> list = generateSampleInputSales();
        inputSalesAdapter = new InputSalesAdapter(list, this);

        rvInputSales.setAdapter(inputSalesAdapter);

    }

    private List<InputSales> generateSampleInputSales() {
        List<InputSales> list = new ArrayList<>();
        list.add(new InputSales("Aceta Star", "90", "20,000", "#56CCF2"));
        list.add(new InputSales("Confidor", "10", "10,000", "#BB6BD9"));
        return list;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode!= RESULT_OK)
            return;

        if(requestCode == REQUEST_PICK_LOCATION){

            if (data != null) {
                handleIntent(data);
            }

        }

    }


    private void handleIntent(Intent data){

        double lat = data.getDoubleExtra("lat",0);
        double lon = data.getDoubleExtra("lng",0);

        findViewById(R.id.layout_map_container).setVisibility(View.VISIBLE);
        animateToMap(new LatLng(lat, lon));

    }
}
