package com.farmerline.farmerservice.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.farmerline.farmerservice.model.InputSalesDistribution;
import com.farmerline.farmerservice.R;

import java.util.ArrayList;
import java.util.List;

import androidx.fragment.app.Fragment;
import lecho.lib.hellocharts.model.Axis;
import lecho.lib.hellocharts.model.AxisValue;
import lecho.lib.hellocharts.model.Column;
import lecho.lib.hellocharts.model.ColumnChartData;
import lecho.lib.hellocharts.model.SubcolumnValue;
import lecho.lib.hellocharts.view.ColumnChartView;

public class InputSalesGraphFragment extends Fragment {

    private ColumnChartView chart;
    private ColumnChartData data;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View rootView = inflater.inflate(R.layout.fragment_column_chart, container, false);

        chart = rootView.findViewById(R.id.chart);

        generateDefaultData();

        return rootView;
    }


    private void generateDefaultData() {
        int numSubcolumns = 1;
        int numColumns;
        List<Column> columns = new ArrayList<>();
        List<InputSalesDistribution> inputSalesDistributions = generateData();
        numColumns = inputSalesDistributions.size();
        List<SubcolumnValue> values;

        List<AxisValue> axisValues = new ArrayList<>();

        for (int i = 0; i < numColumns; ++i) {

            values = new ArrayList<>();
            InputSalesDistribution inputSalesDistribution = inputSalesDistributions.get(i);

            for (int j = 0; j < numSubcolumns; ++j) {
                SubcolumnValue subcolumnValue = new SubcolumnValue((float) getAmount(inputSalesDistribution.getAmount()), Color.parseColor("#0182FF"));
                subcolumnValue.setLabel("Ghs "+inputSalesDistribution.getAmount());
                values.add(subcolumnValue);
            }

            axisValues.add(new AxisValue(i).setLabel(inputSalesDistribution.getName()));


            Column column = new Column(values);
            column.setHasLabels(true);


            columns.add(column);
        }

        data = new ColumnChartData(columns);

        Axis axisX = new Axis();
        Axis axisY = new Axis().setHasLines(true);

        axisX.setName(null);
        axisY.setName("Sales");
        axisY.setValues(generateDataSalesAxis());

        axisX.setValues(axisValues).setTextColor(Color.parseColor("#485465"));
        axisY.setTextColor(Color.parseColor("#485465"));


        data.setAxisXBottom(axisX);
        data.setAxisYLeft(axisY);
        data.setFillRatio(0.5f);
        data.setValueLabelBackgroundEnabled(true);
        data.setValueLabelsTextColor(Color.parseColor("#485465"));
        data.setValueLabelBackgroundColor(Color.parseColor("#00485465"));
        data.setValueLabelBackgroundAuto(false);

        chart.setColumnChartData(data);

    }


    private int getAmount(String amount) {
        return Integer.valueOf(amount.replace(",", ""));
    }

    private List<InputSalesDistribution> generateData(){
        List<InputSalesDistribution> list = new ArrayList<>();
        list.add(new InputSalesDistribution("Input Instant Sales", "28,000"));
        list.add(new InputSalesDistribution("Input Sales on Credit", "44,000"));
        list.add(new InputSalesDistribution("Input Sales of Saving", "13,000"));
        return list;
    }

    private List<AxisValue> generateDataSalesAxis(){
        List<AxisValue> list = new ArrayList<>();
        list.add(new AxisValue(10000).setLabel("10K"));
        list.add(new AxisValue(20000).setLabel("20K"));
        list.add(new AxisValue(30000).setLabel("30K"));
        list.add(new AxisValue(40000).setLabel("40K"));
        list.add(new AxisValue(50000).setLabel("50K"));
        return list;
    }


}